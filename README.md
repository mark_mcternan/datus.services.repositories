# README #

### What is this repository for? ###

* This repo contains ORM wrapper classes for accessing common ORM frameworks, specifically Dapper and EF (Code first).  Both wrappers enable straightforward, strongly typed, testable access to a given datastore while enforcing appropriate transactional control and granularity which makes sense for each ORM's session characteristics.

* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* None

### Who do I talk to? ###

* mark@datus.io